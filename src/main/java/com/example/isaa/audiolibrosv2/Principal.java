package com.example.isaa.audiolibrosv2;

import android.app.AlertDialog;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import android.widget.Toast;

import com.example.spectrum.audiolibrosv2.R;

public class Principal extends AppCompatActivity implements SelectorFragment.OnGridViewListener {

    SelectorFragment.OnGridViewListener mCallback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SelectorAdapter.inicializarVector();
        setContentView(R.layout.principal_activity);

        if(findViewById(R.id.fragment_container)!=null&&savedInstanceState==null){
            SelectorFragment primerFragment=new SelectorFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,primerFragment).commit();
        }

        GridView gridview=(GridView)findViewById(R.id.cuadricula);//la cuadricula del diseño
        gridview.setAdapter(new SelectorAdapter (this));//establecemos el adaptador
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(Principal.this,"Seleccionado el elemento: "+ position,Toast.LENGTH_SHORT).show();

            }


        });



    }

    @Override
    public void onItemSelected(int posicion) {
        DetalleFragment detalleFragment=(DetalleFragment)getSupportFragmentManager().findFragmentById(R.id.detalle_fragment);
        SharedPreferences pref=getSharedPreferences("com.example.audiolibros_internal", MODE_PRIVATE);
        SharedPreferences.Editor editor=pref.edit();
        editor.putInt("position",posicion);
        editor.commit();

        if(detalleFragment!=null){
            detalleFragment.updateBookView(posicion);

        }
        else{
            DetalleFragment nuevoFragment=new DetalleFragment();
            Bundle args=new Bundle();
            args.putInt(DetalleFragment.ARG_POSITION,posicion);
            nuevoFragment.setArguments(args);
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container,nuevoFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_principal,menu);
        SearchManager searchManager=(SearchManager)getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView=(SearchView)menu.findItem(R.id.menu_buscar).getActionView();
        searchView.setQueryHint("Introduce libro o autor");
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_preferencias:
                Toast.makeText(this,"Preferencias",Toast.LENGTH_LONG).show();
                Intent i=new Intent(this,PreferenciasActivity.class);
                startActivity(i);
                break;
            case R.id.menu_ultimo:
                 goToLastVisited();                                                                                                                                                                                                                                                                           goToLastVisited();
                break;
            case R.id.menu_buscar:

                break;
            case R.id.menu_acerca:
                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setMessage("Version 0.01 de AudioLibros");
                builder.setPositiveButton(android.R.string.ok,null);
                builder.create().show();
                break;

        }
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {

        if(intent.getAction().equals(Intent.ACTION_SEARCH)){
            busqueda(intent.getStringExtra(SearchManager.QUERY));
        }

    }

    public void busqueda(String query){
        for(int i=0;i<SelectorAdapter.bookVector.size();i++){
            Datolibro libro=SelectorAdapter.bookVector.elementAt(i);
            if(libro.name.toLowerCase().contains(query.toLowerCase())||(libro.autor.toLowerCase().contains(query.toLowerCase()))){
                onItemSelected(i);

            }
            else{
                Toast.makeText(this,"No  se ha encontrado",Toast.LENGTH_LONG).show();
            }
        }
    }

    public  void goToLastVisited(){
        SharedPreferences pref=getSharedPreferences("com.example.audiolibros_internal",
        MODE_PRIVATE);
        int position=pref.getInt("position",-1);
        if(position>=0){
            onItemSelected(position);
        }
        else{
            Toast.makeText(this,"Sin ultima visita",Toast.LENGTH_LONG).show();
        }

    }
}
