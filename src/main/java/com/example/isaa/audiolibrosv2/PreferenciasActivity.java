package com.example.isaa.audiolibrosv2;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by spectrum on 10/08/17.
 */

public class PreferenciasActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content,new PreferenciasFragment()).commit();
    }
}
