package com.example.isaa.audiolibrosv2;

/**
 * Created by spectrum on 8/08/17.
 */


import android.app.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.spectrum.audiolibrosv2.R;

/**
 * Created by spectrum on 7/08/17.
 */

public class SelectorFragment extends Fragment {
    Activity actividad;
    GridView gridview;
    SelectorAdapter adaptador;
    OnGridViewListener mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        actividad = activity;
        try {
            mCallback = (OnGridViewListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " ha de implementear OngridViewListener");
        }

    }

    public interface OnGridViewListener {
        public void onItemSelected(int position);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.fragment_selector, container, false);
        gridview = (GridView) inflateView.findViewById(R.id.cuadricula);
        adaptador = new SelectorAdapter(actividad);
        gridview.setAdapter(adaptador);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                mCallback.onItemSelected(i);
            }
        });
        gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(actividad);
                CharSequence[] items = {"compartir", "borrar", "insertar"};
                builder.setItems(items, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Toast.makeText(actividad, "compartiendo...", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                startActivity(Intent.createChooser(intent, "¿Con quien compartes?"));
                                break;
                            case 1:
                                SelectorAdapter.bookVector.remove(position);
                                adaptador.notifyDataSetChanged();
                                break;
                            case 2:
                                SelectorAdapter.bookVector.add(SelectorAdapter.bookVector.elementAt(position));
                                adaptador.notifyDataSetChanged();
                                break;
                        }
                    }

                });
                builder.create().show();
                return true;
            }
        });
        return inflateView;
    }
}
