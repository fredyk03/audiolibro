package com.example.isaa.audiolibrosv2;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.spectrum.audiolibrosv2.R;

import java.util.Vector;

/**
 * Created by spectrum on 7/08/17.
 */

public class SelectorAdapter extends BaseAdapter {

    LayoutInflater layoutInflater;
    public static Vector<Datolibro>bookVector;
    public SelectorAdapter(Activity a){
        layoutInflater=(LayoutInflater)a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inicializarVector();
    }
    @Override
    public int getCount() {
        return bookVector.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        TextView audioLibroTextView;
        Datolibro datolibro =bookVector.elementAt(position);
        View view=convertView;
        if(convertView==null){
            view=layoutInflater.inflate(R.layout.elemento_selector,null);

        }
        audioLibroTextView=(TextView)view.findViewById(R.id.titulo);
        imageView=(ImageView) view.findViewById(R.id.imageView1);
        imageView.setImageResource(datolibro.resourceImage);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        audioLibroTextView.setText(datolibro.name);
        return view;

    }
    public  static void inicializarVector(){
        bookVector=new Vector<Datolibro>();

        bookVector.add(new Datolibro("Avecilla","Alas Clarin Leopoldo",R.drawable.avecilla,"http://www.leemp3.com/leemp3/Avecilla_alas.mp3"));
        bookVector.add(new Datolibro("Divina Comedia","Dante",R.drawable.divinacomedia,"http://www.leemp3.com/leemp3/8/Divina%20Comedia_alighier.mp3"));
    }
}
